# Laptop Service
from flask import Flask
from flask_restful import Resource, Api

# Instantiate the app
app = Flask(__name__)
api = Api(app)

# Make project dependant on flask_brevets.py and import the data from it in this own site


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001, debug=True)
