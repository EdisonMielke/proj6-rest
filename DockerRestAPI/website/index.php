<html>
    <head>
        <title>CIS 322 REST-api</title>
    </head>

    <body>
        <h1>List of Brevet Times</h1>
        <ul>
	    <p>All Times</p>
	    <?php
            $json = file_get_contents('http://brevets/listAll');
	    echo str_replace("\n", "<br \>", json_decode($json));
	    echo '<br />';
            ?>
	    <p>Open Times</p>
	    <?php
            $json = file_get_contents('http://brevets/listOpenOnly');
	    echo str_replace("\n", "<br \>", json_decode($json));
	    echo '<br />';
            ?>
            <p>Close Times</p>
	    <?php
            $json = file_get_contents('http://brevets/listCloseOnly');
	    echo str_replace("\n", "<br \>", json_decode($json));
	    echo '<br />';
            ?>
	    <p>All Times(CSV)</p>
	    <?php
            $json = file_get_contents('http://brevets/listAll/csv');
	    echo str_replace("\n", "<br \>", ($json));
	    echo '<br />';
            ?>
	    <p>Open Times(CSV)</p>
	    <?php
            $json = file_get_contents('http://brevets/listOpenOnly/csv');
	    echo str_replace("\n", "<br \>", ($json));
	    echo '<br />';
            ?>
            <p>Close Times(CSV)</p>
	    <?php
            $json = file_get_contents('http://brevets/listCloseOnly/csv');
	    echo str_replace("\n", "<br \>", ($json));
	    echo '<br />';
            ?>
            <p>Open Times(CSV, Top 3)</p>
	    <?php
            $json = file_get_contents('http://brevets/listOpenOnly/csv?top=3');
	    echo str_replace("\n", "<br \>", ($json));
	    echo '<br />';
            ?>
            <p>Close Times(CSV, Top 3)</p>
	    <?php
            $json = file_get_contents('http://brevets/listCloseOnly/csv?top=3');
	    echo str_replace("\n", "<br \>", ($json));
	    echo '<br />';
            ?>
            <p>Open Times(Json, Top 3)</p>
	    <?php
            $json = file_get_contents('http://brevets/listOpenOnly/json?top=3');
	    echo str_replace("\n", "<br \>", json_decode($json));
	    echo '<br />';
            ?>
            <p>Close Times(Json, Top 3)</p>
	    <?php
            $json = file_get_contents('http://brevets/listCloseOnly/json?top=3');
	    echo str_replace("\n", "<br \>", json_decode($json));
	    echo '<br />';
            ?>
        </ul>
    </body>
</html>
