"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request
from flask_restful import Api, Resource, reqparse
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
# client = MongoClient('mongodb://mongodb:27017/')
# client = MongoClient('172.20.0.2', 27017)
# client.server_info() #Used for troubleshooting ip, shouldn't be needed
db = client.appdb


###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    intcheck = request.args.get('km', type=str)
    km = request.args.get('km', 1201, type=float)
    distance = request.args.get('total_distance', type=int)  # added a distance variable so that brevets aren't stuck to
    # 200 km
    datetime = request.args.get('datetime', type=str)  # added a datetime variable based on the date and time from
    # the html file
    app.logger.debug("km={}".format(km))

    errors = ""
    if intcheck.isdigit():
        if int(km) >= 1200:
            errors = "This Brevit checkpoint is past the maximum Brevit length"
            result = {"open": "1999-01-01T00:00:00+00:00", "close": "1999-01-01T00:00:00+00:00", "errors": errors}
            return flask.jsonify(result=result)
        elif int(km) > distance:
            errors = "This Brevit checkpoint is past the Brevit length"
    else:
        errors = "Please enter a number in the 'Miles' or 'Km' Box"
        result = {"open": "1999-01-01T00:00:00+00:00", "close": "1999-01-01T00:00:00+00:00", "errors": errors}
        return flask.jsonify(result=result)

    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, distance, arrow.get(datetime))
    close_time = acp_times.close_time(km, distance, arrow.get(datetime))
    result = {"open": open_time, "close": close_time, "errors": errors}
    return flask.jsonify(result=result)


@app.route("/_submitFunction")
def _submitFunction():
    app.logger.debug("Got a JSON request for: SUBMIT: ")
    item_doc = {
        'open_time_field': request.args['open_time_field'],
        'close_time_field': request.args['close_time_field']
    }

    db.appdb.insert_one(item_doc)
    result = "Submission Added"
    return flask.jsonify(result=result)


@app.route("/display", methods=['POST'])
def _displayFunction():
    app.logger.debug("Got a JSON request for: DISPLAY: ")
    _items = db.appdb.find()
    items = [item for item in _items]
    return flask.render_template('/display.html', items=items)


#############
# HelperFunctions
#############
def extractInts(string):
    return_date = ""
    return_time = ""
    integers = ['1','2','3','4','5','6','7','8','9','0']
    for i in string:
        if i in integers:
            return_time += i
        if i == ' ':
            return_date = return_time
            return_time = ''
    return [int(return_date), int(return_time)]

def topExtract(items):
    top_time = 0
    for i in range(len(items)-1):
        time_a = extractInts(items[i]['open_time_field'])
        time_b = extractInts(items[i+1]['open_time_field'])
        if time_a[0] > time_b[0]:
            top_time = i
        elif time_a[0] < time_b[0]:
            top_time = i+1
        else:
            if time_a[0] > time_b[0]:
                top_time = i
            elif time_a[0] < time_b[0]:
                top_time = i + 1
            else:
                top_time = i

    return top_time
def topExtractClose(items):
    top_time = 0
    for i in range(len(items)-1):
        time_a = extractInts(items[i]['close_time_field'])
        time_b = extractInts(items[i+1]['close_time_field'])
        if time_a[0] > time_b[0]:
            top_time = i
        elif time_a[0] < time_b[0]:
            top_time = i+1
        else:
            if time_a[0] > time_b[0]:
                top_time = i
            elif time_a[0] < time_b[0]:
                top_time = i + 1
            else:
                top_time = i

    return top_time

#############

class listAll(Resource):
    def get(self):
        return_string = ""
        _items = db.appdb.find()
        parser = reqparse.RequestParser()
        parser.add_argument('top')
        args = parser.parse_args()
        arg_value = args.get('top')
        items = [item for item in _items]
        if arg_value is not None and arg_value != "":
            if int(arg_value) > len(items):
                arg_value = len(items)
            for i in range(int(arg_value)):
                j = topExtract(items)
                return_string += f"{items[j]['open_time_field']} {items[j]['close_time_field']}\n"
                items[j]['open_time_field'] = '00 00'
        else:
            for i in range(len(items)):
                return_string += f"{items[i]['open_time_field']} {items[i]['close_time_field']}\n"
        return flask.jsonify(return_string)


class listAllCSV(Resource):
    def get(self):
        return_string = ""
        _items = db.appdb.find()
        parser = reqparse.RequestParser()
        parser.add_argument('top')
        args = parser.parse_args()
        arg_value = args.get('top')
        items = [item for item in _items]
        if arg_value is not None and arg_value != "":
            if int(arg_value) > len(items):
                arg_value = len(items)
            for i in range(int(arg_value)):
                j = topExtract(items)
                return_string += f"{(items[j]['open_time_field'])},{(items[j]['close_time_field'])}\n"
                items[j]['open_time_field'] = '00 00'
        else:
            for i in range(len(items)):
                return_string += f"{(items[i]['open_time_field'])},{(items[i]['close_time_field'])}\n"
        return flask.make_response(return_string)


class listAllJSON(Resource):
    def get(self):
        return_string = ""
        _items = db.appdb.find()
        parser = reqparse.RequestParser()
        parser.add_argument('top')
        args = parser.parse_args()
        arg_value = args.get('top')
        items = [item for item in _items]
        if arg_value is not None and arg_value != "":
            if int(arg_value) > len(items):
                arg_value = len(items)
            for i in range(int(arg_value)):
                j = topExtract(items)
                return_string += f"{items[j]['open_time_field']} {items[j]['close_time_field']}\n"
                items[j]['open_time_field'] = '00 00'
        else:
            for i in range(len(items)):
                return_string += f"{items[i]['open_time_field']} {items[i]['close_time_field']}\n"
        return flask.jsonify(return_string)


api.add_resource(listAll, '/listAll')
api.add_resource(listAllCSV, '/listAll/csv')
api.add_resource(listAllJSON, '/listAll/json')


class listOpenOnly(Resource):
    def get(self):
        return_string = ""
        _items = db.appdb.find()
        parser = reqparse.RequestParser()
        parser.add_argument('top')
        args = parser.parse_args()
        arg_value = args.get('top')
        items = [item for item in _items]
        if arg_value is not None and arg_value != "":
            if int(arg_value) > len(items):
                arg_value = len(items)
            for i in range(int(arg_value)):
                j = topExtractClose(items)
                return_string += f"{items[i]['open_time_field']}\n"
                items[j]['close_time_field'] = '00 00'
        else:
            for i in range(len(items)):
                return_string += f"{items[i]['open_time_field']}\n"
        return flask.jsonify(return_string)


class listOpenOnlyCSV(Resource):
    def get(self):
        return_string = ""
        _items = db.appdb.find()
        parser = reqparse.RequestParser()
        parser.add_argument('top')
        args = parser.parse_args()
        arg_value = args.get('top')
        items = [item for item in _items]
        if arg_value is not None and arg_value != "":
            if int(arg_value) > len(items):
                arg_value = len(items)
            for i in range(int(arg_value)):
                j = topExtractClose(items)
                return_string += f"{items[i]['open_time_field']}\n"
                items[j]['close_time_field'] = '00 00'
        else:
            for i in range(len(items)):
                return_string += f"{items[i]['open_time_field']}\n"
        return flask.make_response(return_string)


class listOpenOnlyJSON(Resource):
    def get(self):
        return_string = ""
        _items = db.appdb.find()
        parser = reqparse.RequestParser()
        parser.add_argument('top')
        args = parser.parse_args()
        arg_value = args.get('top')
        items = [item for item in _items]
        if arg_value is not None and arg_value != "":
            if int(arg_value) > len(items):
                arg_value = len(items)
            for i in range(int(arg_value)):
                j = topExtractClose(items)
                return_string += f"{items[i]['open_time_field']}\n"
                items[j]['close_time_field'] = '00 00'
        else:
            for i in range(len(items)):
                return_string += f"{items[i]['open_time_field']}\n"
        return flask.jsonify(return_string)


api.add_resource(listOpenOnly, '/listOpenOnly')
api.add_resource(listOpenOnlyCSV, '/listOpenOnly/csv')
api.add_resource(listOpenOnlyJSON, '/listOpenOnly/json')


class listCloseOnly(Resource):
    def get(self):
        return_string = ""
        _items = db.appdb.find()
        parser = reqparse.RequestParser()
        parser.add_argument('top')
        args = parser.parse_args()
        arg_value = args.get('top')
        items = [item for item in _items]
        if arg_value is not None and arg_value != "":
            if int(arg_value) > len(items):
                arg_value = len(items)
            for i in range(int(arg_value)):
                j = topExtract(items)
                return_string += f"{items[i]['close_time_field']}\n"
                items[j]['open_time_field'] = '00 00'
        else:
            for i in range(len(items)):
                return_string += f"{items[i]['close_time_field']}\n"
        return flask.jsonify(return_string)


class listCloseOnlyCSV(Resource):
    def get(self):
        return_string = ""
        _items = db.appdb.find()
        parser = reqparse.RequestParser()
        parser.add_argument('top')
        args = parser.parse_args()
        arg_value = args.get('top')
        items = [item for item in _items]
        if arg_value is not None and arg_value != "":
            if int(arg_value) > len(items):
                arg_value = len(items)
            for i in range(int(arg_value)):
                j = topExtract(items)
                return_string += f"{items[i]['close_time_field']}\n"
                items[j]['open_time_field'] = '00 00'
        else:
            for i in range(len(items)):
                return_string += f"{items[i]['close_time_field']}\n"
        return flask.make_response(return_string)


class listCloseOnlyJSON(Resource):
    def get(self):
        return_string = ""
        _items = db.appdb.find()
        parser = reqparse.RequestParser()
        parser.add_argument('top')
        args = parser.parse_args()
        arg_value = args.get('top')
        items = [item for item in _items]
        if arg_value is not None and arg_value != "":
            if int(arg_value) > len(items):
                arg_value = len(items)
            for i in range(int(arg_value)):
                j = topExtract(items)
                return_string += f"{items[i]['close_time_field']}\n"
                items[j]['open_time_field'] = '00 00'
        else:
            for i in range(len(items)):
                return_string += f"{items[i]['close_time_field']}\n"
        return flask.jsonify(return_string)


api.add_resource(listCloseOnly, '/listCloseOnly')
api.add_resource(listCloseOnlyCSV, '/listCloseOnly/csv')
api.add_resource(listCloseOnlyJSON, '/listCloseOnly/json')

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
