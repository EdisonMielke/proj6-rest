# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database.

## What is in this repository

You have a minimal implementation of Docker compose in DockerRestAPI folder, using which you can create REST API-based services (as demonstrated in class). 

## Summary 

Located on Port 5001 is the normal Brevets program, it adds data as normal and can submit/display as normal. Located on Port 5000 is the consumer program which lists a plethora of different times.


CSV values should be shown in the following format DAY mm:dd hh:mm,


Due to the ambiguity of "Ascending" Values, time should be organized with the later time at the bottom and the sooner time at the top
## Grading Rubric

* If your code works as expected: 100 points. This includes:
    * Basic APIs work as expected.
    * Representations work as expected.
    * Query parameter-based APIs work as expected.
    * Consumer program works as expected. 

* For each non-working API, 5 points will be docked off. If none of them work,
  you'll get 35 points assuming
    * README is updated with your name and email ID.
    * The credentials.ini is submitted with the correct URL of your repo.
    * Dockerfile is present. 
    * Docker-compose.yml works/builds without any errors.

* If README is not updated, 5 points will be docked off. 

* If the Docker-compose.yml doesn't build or is missing, 15 points will be
  docked off. Same for Dockerfile as well.

* If credentials.ini is missing, 0 will be assigned.

##Credentials
Author: Edison Mielke

Email: enmielke@gmail.com/edisonm@uoregon.edu